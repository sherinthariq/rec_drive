from django.db import models

# Create your models here.


class CandidateDetails(models.Model):

    name = models.CharField(max_length=30)
    email = models.CharField(max_length=50)
    p_number = models.IntegerField()
    resume = models.ImageField(upload_to='media')

    def __str__(self):
        return self.name
