
from django.shortcuts import render
from .models import CandidateDetails
from django.core.mail import send_mail
from django.template.loader import get_template
# Create your views here.


def index(request):
    count = CandidateDetails.objects.all().count()
    # count=160
    if request.method == 'POST' and request.FILES['resume']:
        check = request.POST.get('check')
        email = request.POST.get('email')
        mail = CandidateDetails.email.objects.all()

        if check == "on":
            return redirect('/')

        else:
            for m in mail:
                if m == email:
                    return redirect('/')

            name = request.POST.get('name')

            p_num = request.POST.get('p_num')
            resume = request.FILES['resume']
            candidate = CandidateDetails(name = name, email = email, p_number=p_num,resume=resume)
            candidate.save()
            context = {'count': count}
            msg = get_template('email_t.html').render(
                        ({
                            'candidate': candidate,
                                                })
                    )
        if count <= 150:
            send_mail(
                'Thanks for registering!',
                msg,
                'py.testtest@gmail.com',
                [candidate.email],
                fail_silently=True,
                html_message=msg
            )
            return render(request,'message.html',context)
        else:
            return render(request, 'message.html',context)

    return render(request,'index.html')






